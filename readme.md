#Badass List of jQuery Plugins

That's right. here I will be maintaining a list of very useful and handy jQuery plugins so that you don't have to search for the appropriate one while working in a project. If you want to add anything new, just fork this repo and send me a pull request. Have Fun!

This list is forked from [Hasin Hayder](http://hasin.me)

**No Commercial plugins, unless it has a free version**

###Environment
* [Detectizr](http://barisaydinoglu.github.io/Detectizr/): A Modernizr extension to detect device, device model, screen size, operating system, and browser details.
* [Head.js](http://headjs.com/): A tiny script, to simplify, speed up, and modernize your site !
* [HTML Inspector](https://github.com/philipwalton/html-inspector):HTML Inspector is a code quality tool to help you and your team write better markup.
* [Jquery Evergreen](http://webpro.github.io/jquery-evergreen/):Small & fast DOM and event library for modern browsers
* [grunticon](https://github.com/filamentgroup/grunticon):grunticon is a Grunt.js task that makes it easy to manage icons and background images for all devices, preferring HD (retina) SVG icons but also provides fallback support for standard definition browsers, and old browsers alike


###Format
* [FormatJS](http://formatjs.io/):Internationalize your web apps on the client & server.

###Fonts
* [FontFace Onload](https://github.com/zachleat/fontfaceonload)

###Polyfills
* [Webshims Lib](http://afarkas.github.io/webshim/demos/index.html): Webshims Lib is a modular capability-based polyfill-loading library, which focuses on accurate implementations of stable HTML5 features

###Carousels
* [Owl Carousel](http://owlgraphic.com/owlcarousel/): A fantastic carousel plugin you can use in your responsive projects
* [slick](http://kenwheeler.github.io/slick/)

###CSS3 Animations & Transitions
* [Transit](http://ricostacruz.com/jquery.transit/): CSS3 Transition and Animation Library
* [Nanimator](http://github.com/hasinhayder/Nanimator): jQuery Nano Animation library for content blocks
* [ElementTransition](http://dan-silver.github.io/ElementTransitions.js/): Simple & beautiful transitions for web pages
* [Oridomi](http://oridomi.com/): Fold the DOM like paper
* [Hover.css](http://ianlunn.github.io/Hover):A collection of CSS3 powered hover effects to be applied to call to actions, buttons, logos, featured images and so on.
* [Transitionize](https://github.com/abpetkov/transitionize):Create CSS3 transitions dynamically with JavaScript
* [Effeckt.css](http://h5bp.github.io/Effeckt.css/)
* [iHover](http://gudh.github.io/ihover/dist/index.html)
* [Bounce.js](http://bouncejs.com/)

###Events
* [TouchSwipe](http://www.awwwards.com/touchswipe-a-jquery-plugin-for-touch-and-gesture-based-interaction.html): A jQuery plugin for touch and gesture-based interaction
* [Drag](http://threedubmedia.com/code/event/drag): A jquery special event plugin that makes the task of adding complex drag interactions, to any element, simple and powerful
* [Manipulator.js](http://iamralpht.github.io/constraints/)
* [KeyPress](http://dmauro.github.io/Keypress/):
* [Hammer.js](http://eightmedia.github.io/hammer.js/): A javascript library for multi-touch gestures.
* [DragDrop](https://github.com/kbjr/DragDrop):A JavaScript micro-framework for adding drag-and-drop functionality to elements for advanced UI development
* [Tiny-pubsub](https://github.com/cowboy/jquery-tiny-pubsub):
* [Jquery.Kinetic](http://davetayls.me/jquery.kinetic/):jQuery.kinetic is a simple plugin which adds smooth drag scrolling with gradual deceleration to containers
* [Perimeter.js](http://github.e-sites.nl/perimeter.js/):Creates an invisible perimeter around a target element and monitors mouse breaches.
* [WhenLive.js](https://github.com/tkambler/whenLive):jQuery plugin that allows you to define event handlers to be fired the moment a specified element becomes available within the DOM.
* [InstantClick.js](http://instantclick.io/): InstantClick is a JavaScript library that dramatically speeds up your website, making navigation effectively instant in most cases.

###Filtering & Sorting
* [Filter](http://www.jscraft.net/plugins/filters.html): A fantastic grid layout library with smart sorting :)
* [MixItUp](http://mixitup.io/): An amazing library for different types of sorting and filtering. You will love it for sure!
* [TableSorter](http://tablesorter.com/docs/): Easy and excellent table sorter plugin.

###Form Elements - Select Box
* [Uniform](http://uniformjs.com/):Custom all elements for forms
* [Choosen](http://harvesthq.github.io/chosen): A jQuery Plugin by Harvest to Tame Unwieldy Select Boxes
* [FancySelect](http://code.octopuscreative.com/fancyselect/): A better select for discerning web developers everywhere.
* [WideArea](http://usablica.github.io/widearea/):WideArea is simple and lightweight JavaScript and CSS library (2KB JS and 4KB CSS) which helps you to write better, simpler and faster.
* [Select-or-Die](https://github.com/vestman/Select-or-Die/)
* [Selectize](http://brianreavis.github.io/selectize.js/)

###Form Validation
* [Parsley](https://github.com/guillaumepotier/Parsley.js): Javascript form validation, without actually writing a single line of javascript.

###Form auto completion
* [jQuery-Autocomplete](https://github.com/devbridge/jQuery-Autocomplete):Ajax Autocomplete for jQuery allows you to easily create autocomplete/autosuggest boxes for text input fields.
* [Fuse.js](http://kiro.me/projects/fuse.html):If you need a lightweight, fast way to search through a list of items, and allow mispellings, then Fuse.js is for you. Forget all server-side logic. This is done all in JavaScript. Check out the demo, and look at the examples below.

### Form range
* [RangeSlider](https://github.com/andreruffert/rangeslider.js)
* [Knob](http://anthonyterrien.com/knob/)
* [PowerRange](http://abpetkov.github.io/powerange/ail)

###Image Processing, Canvas & SVG
* [BlurJS](http://blurjs.com/): A jQuery plugin that produces psuedo-transparent
blurred elements over other elements
* [TiltShift](http://www.noeltock.com/tilt-shift-css3-jquery-plugin/): Obtain Tiltshift/Mniature effect using this cool jQuery Plugin.
* [CropLess](http://www.dictions.org/cropless/): LESS powered auto-cropping
* [Image Cutter](http://www.joustmultimedia.com/blog/post/image_cutter):An open source javascript image cropping plug-in
* [Kinetic.js](http://kineticjs.com/):Canvas framework
* [SVG.js](http://www.svgjs.com/): A lightweight library for manipulating and animating SVG
* [SVGMagic](http://svgmagic.bitlabs.nl/):SVGMagic is a simple jQuery plugin that searchs for SVG images (including background-images) on your website and creates PNG versions if the browser doesn't support SVG.
* [imacss](https://github.com/akoenig/imacss):An application and library that transforms image files to data URIs and embeds them into a single CSS file.
* [html2canvas](http://html2canvas.hertzen.com/)

### Cache
* [jsCache](https://github.com/mortzdk/jsCache)

###Layout
* [Isotope](http://isotope.metafizzy.co/): A complete layout library
* [Masonry](http://masonry.desandro.com/): Layout library for Masonry grids
* [Mason](https://github.com/DrewDahlman/Mason): jQuery Masonry grid with a very useful feature - columns with equal height
* [Freewall](http://vnjs.net/www/project/freewall/): Amazing grid engine!
* [Tether](http://github.hubspot.com/tether/docs/welcome/):Tether is a low-level UI library that can be used to position any element on a page next to any other element.
* [GridList](https://github.com/ubervu/grid):Drag and drop library for two-dimensional, resizable and responsive lists
* [element-query support via JavaScript parser](https://github.com/artlawry/elq)
* [KiteCSS](http://hiloki.github.io/kitecss/):Kite is a flexible layout helper CSS library.

###Lazy Loading
* [Echo.Js](http://toddmotto.com/echo-js-simple-javascript-image-lazy-loading/): Simple JavaScript image lazy loading

###Media Players
* [jPlayer](http://jplayer.org/): The jQuery HTML5 Audio / Video Library
* [Popcorn.js](http://popcornjs.org/): Popcorn.js is an HTML5 media framework written in JavaScript for filmmakers, web developers, and anyone who wants to create time-based interactive media on the web.
* [PrettyEmbed](https://github.com/mike-zarandona/prettyembed.js):Prettier embeds for your YouTubes

###Mouse
* [LazyMouse](http://hasinhayder.github.io/LazyMouse/): Detect Mouse Inactivity easily with this tiny jQuery Plugin.

###Loader
* [CSS3 loaders](http://tobiasahlin.com/spinkit/): CSS3 loaders animation
* [PreloadJS](http://createjs.com/#!/PreloadJS):A Javascript library that lets you manage and co-ordinate the loading of assets.
* [HTML5 loader](http://gianlucaguarini.com/canvas-experiments/jQuery-html5Loader/)

###Scrolling and Parallax
* [Skrollr](http://prinzhorn.github.io/skrollr/):Stand-alone parallax scrolling library for mobile (Android + iOS) and desktop. No jQuery. Just plain JavaScript.
* [Scrollorama](http://johnpolacek.github.io/scrollorama/): The jQuery plugin for doing cool scrolly stuff
* [Setller.js](http://markdalgleish.com/projects/stellar.js/): Parallax has bener been so easier
* [Arbitrary Anchor](http://briangonzalez.org/arbitrary-anchor): ARBITRARY ANCHOR SCROLLING FOR ANY ELEMENT ON YOUR PAGE
* [ScrollTo](http://flesler.blogspot.com/2007/10/jqueryscrollto.html): A simple jQuery plugin for scrolling elements, or the window itself.
* [Sticky Kit](http://leafo.net/sticky-kit/): Sticky-kit provides an easy way to attach elements to the page when the user scrolls such that the element is always visible.
* [FullPage](http://alvarotrigo.com/fullPage): Create Beautiful Fullscreen Scrolling Websites
* [ScrollMagic](http://janpaepke.github.io/ScrollMagic/)

###Sliders
* [Swiper](http://www.idangero.us/sliders/swiper/): Mobile touch slider & framework with hardware accelerated transitions
* [SwipeJS](http://swipejs.com/): Swipe is the most accurate touch slider
* [Responsive JS](http://responsive-slides.viljamis.com): A tiny responsive slider plugin with some cool features, in 1KB only
* [Sequence JS](http://www.sequencejs.com/): The Slider Reimagined for the Modern Web, comes with fantastic CSS3 transitions and parallux
* [Supersized](http://buildinternet.com/project/supersized): Fullscreen background slideshow plugin
* [iView](http://iprodev.com/iview/): Beautiful and slick slider plugin which is responsive too
* [Orbit](https://github.com/zurb/orbit): Another beautiful slider plugin from Zurb
* [Superslider](https://github.com/nicinabox/superslides): A fullscreen, hardware accelerated slider for jQuery
* [SliderJS](http://www.slidesjs.com/): SlidesJS is a responsive slideshow plug-in for jQuery (1.7.1+) with features like touch and CSS3 transitions.

###Sound & Audio
* [Ion.Sound](http://ionden.com/a/plugins/ion.sound/en.html): Nifty jQuery library to play sound on events
* [Annyang](https://www.talater.com/annyang/):Voice website controller
* [Wad](https://github.com/rserota/wad):Web Audio DAW. Use the HTML5 Web Audio API for dynamic sound synthesis. It's like jQuery for your ears.

### Maps
* [Maplace.Js](http://maplacejs.com/):A small Google Maps Javascript plugin for jQuery
* [Leaflet](http://leafletjs.com/):An Open-Source JavaScript Library for Mobile-Friendly Interactive Maps

###UI Elements - Calendar
* [CLNDR](http://kylestetz.github.io/CLNDR/): A jQuery Calendar Plugin

####UI Icons
* [CSS3 Icons](http://www.uiplayground.in/css3-icons/):We create beautiful icons using CSS/3, which makes your site light and reduce http request


###UI Elements - Images & Photos
* [TwentyTwenty - Before & After Differences](http://zurb.com/playground/twentytwenty): TwentyTwenty is a image difference tool which shows you the difference between two images visually
* [Adipoli](http://cube3x.com/adipoli-jquery-image-hover-plugin/): Adipoli is a simple jQuery plugin used to bring stylish image hover effects
* [BackStretch](http://srobbin.com/jquery-plugins/backstretch/): a simple jQuery plugin that allows you to add a dynamically-resized, slideshow-capable background image to any page or element
* [Taggd](http://timseverien.nl/taggd): Taggd is a jQuery plugin that help you create tags on images with, or without a popup!
* [jGallery](http://jgallery.jakubkowalczyk.pl/): Beautiful jQuery photo gallery with albums and preloader
* [Pongstagr.am](http://pongstr.github.io/pongstagr.am/): A jQuery plugin that lets you display your Instagram media to your website using Bootstrap Front-end styles and modal-plugin.
* [favico.js](http://lab.ejci.net/favico.js/):Make a use of your favicon with badges, images or videos

###UI Elements - Lightboxes & Modals
* [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/): Magnific Popup is a responsive jQuery lightbox plugin that is focused on performance and providing best experience for user with any device
* [FlipLightBox](http://flipgallery.net/fliplightbox.html): Responsive Lightbox jQuery Plugin
* [PrettyPhoto](http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/): PrettyPhoto is a jQuery lightbox clone. Not only does it support images, it also support for videos, flash, YouTube, iframes and ajax. It’s a full blown media lightbox.

###UI Elements - Others
* [JoyRide - Feature Tour Plugin](http://zurb.com/playground/jquery-joyride-feature-tour-plugin): As said, it's a beautiful feature tour plugin by Zurb.
* [FlowType](http://simplefocus.com/flowtype/): Web typography at its finest: font-size and line-height based on element width.
* [Hubspot Messaging Library](http://github.hubspot.com/messenger/): To show transactional messages in your app



###UI Elements - Responsive Menu
* [FlexiNav](http://jasonweaver.name/lab/flexiblenavigation/): A Device-Agnostic Approach to Complex Site Navigation.
* [SlimMenu](http://adnantopal.github.io/slimmenu): Another multilevel responsive menu

###UI Elements - Scrollers
* [NanoScroller.js](http://jamesflorentino.github.io/nanoScrollerJS/): nanoScroller.js is a jQuery plugin that offers a simplistic way of implementing Mac OS X Lion-styled scrollbars for your website.
* [NiceScroll](http://areaaperta.com/nicescroll/): Nicescroll is a jquery plugin, for nice scrollbars with a very similar ios/mobile style.

###UI Elements - Social Sharing
* [jQuery Tweetable](https://github.com/jmduke/jquery.tweetable.js): A simple li'l plugin that lets you make site content easily tweetable.

###UI Elements - Tables
* [TableCloth.js](http://tableclothjs.com/): Tablecloth.js is a jQuery plugin that helps you easily style HTML tables along with some simple customizations.

###UI Elements - Toolbar
* [Toolbar.js](http://paulkinzett.github.io/toolbar/): A jQuery plugin that creates tooltip style toolbars


###UI Elements - Tabs
* [Tabulous.js](http://git.aaronlumsden.com/tabulous.js): A fantastic jQuery plugin to help you create tabs, easily
* [NanoTabs](www.sunsean.com/nanotabs/): That's right - beautiful tabs in a nano sized plugin.

###UI Elements - WYSIWYG
* [SirTrevor](http://madebymany.github.io/sir-trevor-js/): A beautiful rich content editor reimagined for web.

###SASS
* [SassyJSON](https://github.com/HugoGiraudel/SassyJSON):A Sass API for JSON.
